/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Fichero: Ejercicio0604.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
public class Ejercicio0604 {

  public static String remove(String input) {
    // Cadena de caracteres original a sustituir.
    String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
    // Cadena de caracteres ASCII que reemplazarán los originales.
    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
    String output = input;
    for (int i = 0; i < original.length(); i++) {
      // Reemplazamos los caracteres especiales.
      output = output.replace(original.charAt(i), ascii.charAt(i));
    }//for i
    return output;
  }//remove

  public static void main(String[] args) throws Exception {
    File fr = new File(args[0]);
    File fw = new File(args[0] + ".txt");
    BufferedReader br = new BufferedReader(
            new FileReader(fr));
    BufferedWriter bw = new BufferedWriter(
            new FileWriter(fw));
    String s;
    while ((s = br.readLine()) != null) {
      s = remove(s);
      bw.write(s, 0, s.length());
      bw.write("\r\n");
    }
    br.close();
    bw.close();
    fr.delete();
    fw.renameTo(fr);
  }
}
