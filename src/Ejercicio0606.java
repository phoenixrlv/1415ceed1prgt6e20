/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Fichero: Ejercicio0606.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
public class Ejercicio0606 {

  public static void main(String args[]) {

    // Escribiendo
    String s1 = "Texto";
    File f1 = new File("texto.txt");

    try {
      FileWriter fw = new FileWriter(f1);
      for (int i = 0; i < s1.length(); i++) {
        fw.write(s1.charAt(i));
        //fw.write("\r\n");
      }
      if (fw != null) {
        fw.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    // Leyendo
    File f2 = new File("texto.txt");
    if (f2.exists()) {
      try {
        FileReader fr = new FileReader(f2);
        BufferedReader br = new BufferedReader(fr);
        int c2;
        while ((c2 = br.read()) != -1) {
          System.out.print((char) c2);
        }
        if (fr != null) {
          br.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
