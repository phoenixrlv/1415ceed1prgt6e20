/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejercicio0601.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
public class Ejercicio0601 {

  public static void main(String[] args) throws IOException {
    InputStreamReader conversor;
    BufferedReader teclado;
    String linea;
    conversor = new InputStreamReader(System.in);
    teclado = new BufferedReader(conversor);
    System.out.print("Introduce un byte: ");
    linea = teclado.readLine();
    byte b = Byte.parseByte(linea);
    System.out.println("El valor leido fue: " + b);
    System.out.print("Introduce un int ");
    linea = teclado.readLine();
    int i = Integer.parseInt(linea);
    System.out.println("El valor leido fue: " + i);
    System.out.print("Introduce un double ");
    linea = teclado.readLine();
    double d = Double.parseDouble(linea);
    System.out.println("El valor leido fue: " + d);
    boolean leido;
    do {
      try {
        System.out.println("Introduce int valido: ");
        linea = teclado.readLine();
        i = Integer.parseInt(linea);
        leido = true;
      } catch (NumberFormatException e) {
        System.out.println("Numero no valido: ");
        leido = false;
      }
    } while (!leido);
  }
}
/* EJECUCION:
 Introduce un byte: 1
 El valor leido fue: 1
 Introduce un int 2
 El valor leido fue: 2
 Introduce un double 2.0
 El valor leido fue: 2.0
 Introduce int valido: 
 1
 */
