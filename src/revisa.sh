
# Bash Script 
# Por Paco Aldarias.

rm *.java

cp ~/Dropbox/ceed1314/apuntes/NetBeansProjects/t8/prgt8e20aldarias/src/prgt8e20aldarias/* .

echo "Renombrando ficheros con Ejemplo08 por Ejemplo06"
rename -v 's/Ejercicio08/Ejercicio06/' *.java

for i in $(ls *.java);do 
 echo $i
 
 echo "Borrando lineas con el texto package"
 cat $i | grep  -Ev package > t$i 
 mv t$i $i
 
 echo "Reemplazando el texto Ejemplo08 por Ejemplo06"
 cat $i | sed 's/Ejercicio08/Ejercicio06/g' > t$i
 mv  t$i $i
done   
