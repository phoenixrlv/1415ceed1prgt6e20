/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;

/**
 * Fichero: Ejercicio0603.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
public class Ejercicio0603 {

  public static void main(String args[]) throws IOException {
    // Se comprueba que nos han indicado algun fichero
    if (args.length > 0) {
      // Vamos comprobando cada uno de los ficheros que se hayan pasado
      // en la linea de comandos
      for (int i = 0; i < args.length; i++) {
        // Se crea un objeto File para tener una referencia al fichero
        // fisico del disco
        File f = new File(args[i]);

        // Se presenta el nombre y directorio donde se encuentra
        System.out.println("Nombre: " + f.getName());
        System.out.println("Camino: " + f.getPath());
        // Si el fichero existe se presentan los permisos de lectura y
        // escritura y su longitud en bytes
        if (f.exists()) {
          System.out.print("Fichero existente");
          System.out.print((f.canRead() ? " y se puede Leer" : ""));
          System.out.print((f.canWrite() ? " y se puede Escribir" : ""));
          System.out.println(".");
          System.out.println("La longitud del fichero es de "
                  + f.length() + " bytes");
        } else {
          System.out.println("El fichero no existe.");
        }
      }
    } else {
      System.out.println("Debe indicar un fichero.");
    }
  }
}
/* EJECUCION
 Nombre: FileInfo.java
 Camino: FileInfo.java
 Fichero existente y se puede Leer y se puede Escribir.
 La longitud del fichero es de 1482 bytes
 */
