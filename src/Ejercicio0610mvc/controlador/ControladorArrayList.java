package Ejercicio0610mvc.controlador;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import Ejercicio0610mvc.modelo.Cliente;
import Ejercicio0610mvc.vista.Vista;
import Ejercicio0610mvc.vista.VistaArrayList;
import java.io.IOException;
import java.util.ArrayList;
import util.Util;

/**
 * Fichero: ControladorArray.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class ControladorArrayList {

    public void menu(ArrayList<Cliente> ClientesArrayList)
        throws IOException {

        VistaArrayList vistaArrayList = new VistaArrayList();
        Vista vista = new Vista();
        Util util = new Util();
        int ope;

        do {
            ope = vista.opcionO();
            switch (ope) {
                case 0:  // Grabar Fichero
                    vista.fin();
                    break;
                case 1: // Añadir
                    vistaArrayList.getClientes(ClientesArrayList);
                    break;
                case 2:  // Listar
                    vistaArrayList.showArticulos(ClientesArrayList);
                    break;
                case 3:  // Leer Fichero
                    vistaArrayList.leerFichero(ClientesArrayList);
                    break;
                case 4:  // Grabar Fichero
                    vistaArrayList.grabarFichero(ClientesArrayList);
                    break;
                default:
                    vista.error(1);
            }
        } while (ope != 0);

    }
}
