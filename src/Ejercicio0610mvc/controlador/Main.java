/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0610mvc.controlador;

import Ejercicio0610mvc.modelo.Cliente;
import Ejercicio0610mvc.vista.Vista;
import java.io.IOException;
import java.util.ArrayList;
import util.Util;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class Main {

    final public static int TALLA = 3;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        int ope;

        Util util = new Util();
        Vista vista = new Vista();

        ControladorArrayList controladorArrayList = new ControladorArrayList();
        ArrayList<Cliente> clientesArrayList = new ArrayList<>();
        controladorArrayList.menu(clientesArrayList);

    }
}
