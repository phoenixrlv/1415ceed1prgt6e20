/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0610mvc.vista;

import Ejercicio0610mvc.controlador.Main;
import Ejercicio0610mvc.modelo.Cliente;
import Ejercicio0610mvc.util.Constantes;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;
import util.Util;

/**
 * Fichero: VistaArticulo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class VistaArrayList {

    public void getClientes(ArrayList<Cliente> clientesArrayList)
        throws IOException {

        Cliente cliente;
        Vista vista = new Vista();
        Util util = new Util();
        Main main = new Main();
        String nombre, email;
        int edad;
        int correcto;

        System.out.println("VISTA ARRAYLIST");
        System.out.println("TOMA DE DATOS");

        nombre = "";
        edad = 0;
        email = "";

        System.out.print("Nombre: ");
        nombre = util.pedirString();

        while (edad <= 0) {
            System.out.print("Edad: ");
            edad = util.pedirInt();
            if (edad <= 0) {
                vista.error(2);
            }
        }

        correcto = 1;
        while (correcto == 1) {
            System.out.print("Email: ");
            email = util.pedirString();
            if (util.invalidoEmail(email) == true) {
                correcto = 0;
            } else {
                correcto = 1;
                vista.error(3);
            }
        }

        cliente = new Cliente(nombre, edad, email);
        clientesArrayList.add(cliente);

    }

    public void showArticulos(ArrayList<Cliente> clientesArrayList) {

        System.out.println("VISTA ARRAYLIST");
        System.out.println("MOSTRANDO DATOS");
        Iterator it = clientesArrayList.iterator();
        while (it.hasNext()) {
            Cliente articulo = (Cliente) it.next();
            System.out.println(articulo.getNombre()
                + " " + articulo.getEdad()
                + " " + articulo.getEmail());
        }

    }

    public void grabarFichero(ArrayList<Cliente> clientesArrayList) {

        File fs = new File(Constantes.NOMBREFICHERO);
        try {
            FileWriter fw = new FileWriter(fs);
            for (Cliente cli : clientesArrayList) {

                fw.write(cli.getNombre(), 0, cli.getNombre().length());
                fw.write(";", 0, 1);
                fw.write(Integer.toString(cli.getEdad()), 0, 1);
                fw.write(";", 0, 1);
                fw.write(cli.getEmail(), 0, cli.getEmail().length());
                fw.write("\r\n");
            }
            if (fw != null) {
                fw.close();
            }
        } catch (IOException e) {
        }

    }

    public void leerFichero(ArrayList<Cliente> clientesArrayList) {

        Constantes constante = new Constantes();
        File fs = new File(constante.NOMBREFICHERO);
        StringTokenizer stringTokenizer;
        Cliente cliente;

        if (fs.exists()) {
            try {
                FileReader fr = new FileReader(fs);
                BufferedReader br = new BufferedReader(fr);
                String linea;

                while ((linea = br.readLine()) != null) {
                    stringTokenizer = new StringTokenizer(linea, ";");

                    String nombre;
                    int edad;
                    String email;

                    nombre = stringTokenizer.nextToken();
                    edad = Integer.parseInt(stringTokenizer.nextToken());
                    email = stringTokenizer.nextToken();

                    cliente = new Cliente(nombre, edad, email);
                    clientesArrayList.add(cliente);

                } // while

                if (fr != null) {
                    fr.close();
                }

            } catch (IOException e) {
            }
        } // if

    } // leerFichero
}
